  
  $(function () {
  			$('[data-toggle="tooltip"]').tooltip();
  			$('[data-toggle="popover"]').popover();

  			// Configuracion del carousel, podemos pasar parametros...
  			$('.carousel').carousel({
  				interval: 2000
  			});

  			// Apuntamos al boton "contacto" que esta en hotel Alexa...
  			// usamos on - para suscribir eventos...
  			// Eventos sobre el modal...

  			$('#contacto').on('show.bs.modal', function(e){
  				console.log("El modal se esta mostrando");

  				// Cambiamos la clase al boton....

  				$('#contactoBtn').removeClass('btn-outline-success');
  				$('#contactoBtn').addClass('btn-primary');
  				$('#contactoBtn').prop('disable', true);

  			});

  			 $('#contacto').on('shown.bs.modal', function(e){
  				console.log("El modal se ha mostrado");
  			});


  			$('#contacto').on('hide.bs.modal', function(e){
  				console.log("El modal se oculta");

  			});

  			 $('#contacto').on('hidden.bs.modal', function(e){
  				console.log("El modal se oculto");

  				// Cambiamos la clase al boton....

  				$('#contactoBtn').removeClass('btn-primary');
  				$('#contactoBtn').addClass('btn-outline-success');
  				$('#contactoBtn').prop('disable', false);

  			});

		})
