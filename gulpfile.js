'use strict'

var gulp = require('gulp'),   // importar gulp
	sass = require('gulp-sass'),  // importar plugin
	browserSync = require('browser-sync'); // importar plugin


// Procesa todos los archivos scss y los conviernte a css en la carpeta destino

function 


gulp.task('sass', function(){
	gulp.src('./css/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest('./css'));     // Los devuelve en la misma carpeta...
})


//Aqui le indicamos que vigile los archivos sass, en caso de cambios, corre la tarea sass para crear nuevos css...
gulp.task('sass:watch', function(){
	gulp.watch('./css/*.scss', ['sass']);
});

// Renicia el servidor cada vez que tenemos cambios...
gulp.task('browser-sync', function(){
	var files = ['./*.html', './css/*.css', './images/*.{png, jpg, gif}', './js/*.js']
	browserSync.init(files, {
		server: {
			baseDir: './'
		}
	})
});


function defaultTask(cb){
	cb();

}

exports.default = defaultTask





